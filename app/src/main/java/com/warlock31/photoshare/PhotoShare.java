package com.warlock31.photoshare;

import android.app.Application;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.warlock31.photoshare.models.Photo;
import com.warlock31.photoshare.models.PhotoTarget;

/**
 * Created by Warlock on 12/30/2015.
 */
public class PhotoShare extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        Log.e("Messsage", "Photo Share Entered");

        Parse.initialize(this, "h1UmoVFCzM3E8LNPilEZ4QyBLihJ3Qu6axHkJtx1", "59KkBks6KcxlKSFcAu3kWnEj63lD4WjNkjUr0IJF");
        ParseFacebookUtils.initialize(this);
        ParseObject.registerSubclass(Photo.class);
        ParseObject.registerSubclass(PhotoTarget.class);
    }
}
