package com.warlock31.photoshare;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Warlock on 12/31/2015.
 */
public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (ParseUser.getCurrentUser() != null){
            onLoginSuccess();
            Toast.makeText(this,"Logged in",Toast.LENGTH_SHORT).show();
        }
        else {
            setContentView(R.layout.activity_login);
            ButterKnife.bind(this);
        }

    }

    @SuppressWarnings("unused")
    @OnClick(R.id.login_button)
    public void onFacebookButtonClicked(){
        Log.e("Messsage","Entered");
        ArrayList<String> permissions = new ArrayList<>(Arrays.asList(new String[]{"email"}));

        ParseFacebookUtils.logInWithReadPermissionsInBackground(this, permissions, new LogInCallback() {
            @Override
            public void done(ParseUser user, com.parse.ParseException e) {
                if (e == null) {
                    onLoginSuccess();
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        ParseFacebookUtils.onActivityResult(requestCode,resultCode,data);
    }

    private void onLoginSuccess(){
        Log.e("Messsage","Success");
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
