package com.warlock31.photoshare;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.warlock31.photoshare.models.Photo;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private int GALLERY_REQUEST_CODE = 2312;

    ///@Bind(R.id.main_take_photo_button) Button mTakePhotoButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Button takePhotoButton = (Button) findViewById(R.id.main_take_photo_button);
        Button viewPhotoButton = (Button) findViewById(R.id.main_view_photos);

    }

    @OnClick(R.id.main_take_photo_button)
    public void onTakePhotoButtonClicked() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE);
    }

    @OnClick(R.id.main_view_photos)
    public void onViewPhotosButtonClicked(){
        startActivity(new Intent(this, PhotoShareApplication.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null) {
                byte[] pictureContents = loadImage(data.getData());

                if (pictureContents != null) {
                    Photo photo = new Photo();
                    photo.setPhoto(new ParseFile(pictureContents));
                    photo.setPhotographer(ParseUser.getCurrentUser());

                    photo.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Toast.makeText(MainActivity.this, "Successfully saved photo", Toast.LENGTH_SHORT).show();
                                Log.e("Photo Saved","Success");
                            } else {
                                e.printStackTrace();
                            }
                        }
                    });
                }

            }
        }
    }

    private byte[] loadImage(Uri pathToImage) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream fileInputStream = null;
        try {
            fileInputStream = getContentResolver().openInputStream(pathToImage);
            byte[] buffer = new byte[1024];

            while ((fileInputStream.read(buffer)) != -1)
                outputStream.write(buffer, 0, buffer.length);

            return outputStream.toByteArray();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        return null;

    }


}
