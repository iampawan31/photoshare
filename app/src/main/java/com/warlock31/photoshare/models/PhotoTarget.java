package com.warlock31.photoshare.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Created by Warlock on 1/5/2016.
 */

@ParseClassName("PhotoTarget")
public class PhotoTarget extends ParseObject {

    public static ParseQuery<PhotoTarget> getQuery(){
        return new ParseQuery<PhotoTarget>("PhotoTarget");
    }

    public ParseUser getTarget(){
        return getParseUser("target");
    }

    public void setTarget(ParseUser target){
        put("target",target);
    }

    public Photo getPhoto(){
        return  (Photo) get("photo");
    }

    public void setPhoto(Photo photo){
        put("photo",photo);
    }
}
